package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"time"
)

type News struct {
	ID          int       `json:"id"`
	Title       string    `json:"title"`
	Body        string    `json:"body"`
	Provider    string    `json:"provider"`
	PublishedAt time.Time `json:"published_at"`
	Tickers     []string  `json:"tickers"`
}

type CompanyNewsPayload struct {
	Items        []News    `json:"items"`
	Published_at time.Time `json:"published_at"`
	Tickers      []string  `json:"tickers"`
}

type Feed struct { //out structure
	Type    string      `json:"type"`
	Payload interface{} `json:"payload"`
}

func TickersCompare(tickers1, tickers2 []string) bool {
	set := make(map[string]bool)

	if len(tickers1) != len(tickers2) {
		return false
	}
	for _, ticker1 := range tickers1 {
		set[ticker1] = true
	}
	for _, ticker2 := range tickers2 {
		if !set[ticker2] {
			return false
		}
	}
	return true
}

func JSONReadAndSorter(file string) ([]News, error) {
	jsonFile, err := os.Open(file)
	if err != nil {
		return nil, fmt.Errorf("failed to open file %s", err)
	}

	fmt.Println("Successfully Opened: ", file)
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	var news []News
	json.Unmarshal(byteValue, &news)

	sort.Slice(news, func(i, j int) bool {
		return news[i].PublishedAt.Before(news[j].PublishedAt)
		// why this is not working: news[i].PublishedAt < news[j].PublishedAt
	})
	return news, nil
}

func Groupper(sorted []News) ([]Feed, error) {
	var out []Feed
	var items []News
	indexed := make(map[int]bool)

	for _, story := range sorted {
		for _, comp := range sorted {
			if story.PublishedAt.Day() == comp.PublishedAt.Day() &&
				TickersCompare(story.Tickers, comp.Tickers) && !indexed[comp.ID] { //&& story.ID != comp.ID
				items = append(items, comp)
				indexed[comp.ID] = true
			}

		}

		if len(items) > 1 {
			payload := CompanyNewsPayload{Items: items,
				Published_at: items[0].PublishedAt,
				Tickers:      story.Tickers}
			out = append(out, Feed{Type: "company_news", Payload: payload})
		} else if len(items) == 1 {
			out = append(out, Feed{Type: "news", Payload: story})
		}

		items = nil
	}
	return out, nil
}

func JSONCreate(feed []Feed, filename string) error {
	fmt.Println("Start writing", filename)
	marshalled, err := json.MarshalIndent(feed, "", "    ")
	if err != nil {
		return fmt.Errorf("marshal error: %s", err)
	}

	file, err := os.Create(filename)
	if err != nil {
		return fmt.Errorf("can't handle file: %s", err)
	}
	defer file.Close()

	writer := bufio.NewWriter(file)
	defer writer.Flush()

	_, err = writer.Write(marshalled)
	if err != nil {
		return fmt.Errorf("cant write marshalled data to file: %s", err)
	}

	fmt.Println("Successfully saved: ", filename)

	return nil
}

func main() {
	file := flag.String(
		"file",
		"/path/to/your/file",
		"specifies path to json news file")
	flag.Parse()

	Sorted, err := JSONReadAndSorter(*file)
	if err != nil {
		log.Fatalf("Failed to read json")
	}
	GrouppedAndSorted, err := Groupper(Sorted)
	if err != nil {
		log.Fatalf("failed to sort json")
	}

	errr := JSONCreate(GrouppedAndSorted, "out.json")
	if errr != nil {
		log.Fatalf("failed to create json")
	}
}
